const { json } = require('express');
const express = require('express');
const Todo = require('../models/todo');

//RESFULL = GET, POST, PUT, PACH Y DELETE
//Modelo = Una estructura de datos que representa una entidad real
function list(req, res, next){
    Todo.find().then(objs => res.status(200).json({
        message: res._('ok.list'),
        obj: objs
    })).catch(ex => res.status(500).json({
        message: res._('bad.list'),
        obj:ex
    }));
}

function index(req, res, next){
    const id = req.params.id;
    console.log("b");
    Todo.findOne({"_id":id}).then(obj => res.status(200).json({
        message: res._('ok.index'),
        obj: obj
    })).catch(ex => res.status(500).json({
        message: res._('bad.index'),
        obj: ex
    }));
}

function create(req, res, next){
    const text= req.body.text;

    let todo = new Todo({
        text:text
    });

    todo.save().then(obj => res.status(200).json({
        message: res._('ok.create'),
        obj: obj
    })).catch(ex => res.status(500).json({
        message: res._('bad.create'),
        obj:ex
    }));
}

function replace(req, res, next){
    const id = req.params.id;
    const text= req.body.text ? req.body.text: "";

    let todo = new Object({
        _text: text
    });

    Todo.findOneAndUpdate({"_id":id},todo).then(obj => res.status(200).json({
        message: res._('ok.replace'),
        obj:obj
    })).catch(ex => res.status(500).json({
        message: res._('bad.replace'),
        obj:ex
    }));
}

function edit(req, res, next){
    const id = req.params.id;
    const text= req.body.text;

    let todo = new Object();

    if(text){
        todo._text = text;
    }

    Todo.findOneAndUpdate({"_id":id},todo).then(obj => res.status(200).json({
        message:res._('ok.edit'),
        obj:obj
    })).catch(ex => res.status(500).json({
        message: res._('bad.edit'),
        obj:ex
    }));

}

function destroy(req, res, next){
    const id = req.params.id;

    Todo.remove({"_id":id}).then(obj => res.status(200).json({
        message: res._('ok.destroy'),
        obj:obj
    })).catch(ex => res.status(500).json({
        message: res._('bad.destroy'),
        obj:ex
    }));
}

module.exports = {
    list, index, create, replace, edit, destroy
}
