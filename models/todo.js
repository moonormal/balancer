const mongoose = require('mongoose');

const schema = mongoose.Schema({
    _text:String
});

class Todo {
    
    constructor(text){
        this._text=text;
    }

    get text(){
        return this._text;
    }

    set text(v){
        this._text=v;
    }

}

schema.loadClass(Todo);
module.exports = mongoose.model('Todo', schema);